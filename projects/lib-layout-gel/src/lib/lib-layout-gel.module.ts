import { NgModule } from '@angular/core';
import { LayoutGelComponent } from './layout-gel/layout-gel.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BotonesComponent } from './botones/botones.component';
import { MenuComponent } from './header/menu/menu.component';

import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [
    LayoutGelComponent,
    HeaderComponent,
    FooterComponent,
    BotonesComponent,
    MenuComponent
  ],
  imports: [
    NzGridModule,
    NzDividerModule,
    NzSpaceModule,
    FontAwesomeModule,
    NzDropDownModule,
    NzButtonModule,
    NzIconModule,
    NzInputModule,
    NzMenuModule,
    CommonModule
  ],
  exports: [
    LayoutGelComponent
  ]
})
export class LibLayoutGelModule { }
