import { TestBed } from '@angular/core/testing';

import { LibLayoutGelService } from './lib-layout-gel.service';

describe('LibLayoutGelService', () => {
  let service: LibLayoutGelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LibLayoutGelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
