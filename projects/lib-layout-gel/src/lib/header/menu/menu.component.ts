import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lib-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  opcionesPrincipales: any[] = [
    {
      id: '1',
      label: 'Herramienta Anticorrupción',
      codigo: 'herramienta',
      url: '#',
      opcionesSecundarias: [
        {
          id: '1',
          label: 'Plan Anticorrupción y Atención al Ciudadano',
          codigo: 'planAnticorrupcion',
          url: '#',
        },
        {
          id: '2',
          label: 'Diagnostico',
          codigo: 'diagnostico',
          url: '#',
        },
        {
          id: '3',
          label: 'Seguimiento',
          codigo: 'seguimiento',
          url: '#',
        },
        {
          id: '4',
          label: 'Indicador de herramienta de gestión pública para la prevención de la corrupción',
          codigo: 'indicador',
          url: '#',
        },
      ],
    },
    {
      id: '2',
      label: 'Políticas públicas',
      codigo: 'politicasPublicas',
      url: '#',
      opcionesSecundarias: [
        {
          id: '1',
          label: 'Gestión de Riesgos',
          codigo: 'riesgos',
          url: '#',
        },
        {
          id: '2',
          label: 'Racionalización de Trámites',
          codigo: 'tramites',
          url: '#',
        },
        {
          id: '3',
          label: ' Rendición de Cuentas',
          codigo: 'cuentas',
          url: '#',
        },
        {
          id: '4',
          label: 'Mecanismos para Mejorar la Atención al Ciudadano',
          codigo: 'atencionCiudadano',
          url: '#',
        },
        {
          id: '5',
          label: 'Mecanismos para la Transparencia y el Acceso a la Información',
          codigo: 'accesoInformacion',
          url: '#',
        },
        {
          id: '6',
          label: 'Iniciativas Adicionales',
          codigo: 'iniciativasAdicionales',
          url: '#',
        },
      ],
    },
    {
      id: '3',
      label: 'Participación Ciudadana',
      codigo: 'participacion',
      url: '#',
      opcionesSecundarias: [
        {
          id: '1',
          label: 'Calendario de participación',
          codigo: 'calendario',
          url: '#',
        },
        {
          id: '2',
          label: 'Encuestas de opinión',
          codigo: 'encuestas',
          url: '#',
        },
        {
          id: '3',
          label: 'Denuncias',
          codigo: 'denuncias',
          url: '#',
        },
        {
          id: '4',
          label: 'Únete a la conversación',
          codigo: 'conversacion',
          url: '#',
        },
      ],
    },
    {
      id: '4',
      label: 'Normativa',
      codigo: 'normativa',
      url: '#',
      opcionesSecundarias: [
        {
          id: '1',
          label: 'Guías',
          codigo: 'guias',
          url: '#',
        },
        {
          id: '2',
          label: 'Normativas',
          codigo: 'normativas',
          url: '#',
        },
        {
          id: '3',
          label: 'Glosario',
          codigo: 'glosario',
          url: '#',
        },
        {
          id: '4',
          label: 'Biblioteca',
          codigo: 'biblioteca',
          url: '#',
        },
      ],
    },
  ];
  constructor() {}

  ngOnInit(): void {}

  change(value: boolean): void {
    console.log(value);
  }
}
