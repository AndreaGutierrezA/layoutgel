/*
 * Public API Surface of lib-layout-gel
 */

export * from './lib/lib-layout-gel.service';
export * from './lib/layout-gel/layout-gel.component';
export * from './lib/lib-layout-gel.module';
